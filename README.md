# Introduction to the UNIX Shell

August 4/5

Instructor: [Francis Kayiwa](https://twitter.com/kayiwa)

## Overview

This will be a half day workshop to introducing you to the command line. The workshop will focus on the user and programming interface of the UNIX Operating System. The target audience for the workshops is neophytes who are completely new to the command line and unix. The structure of the workshop is to remain self-driven with some guidance. This nature of this repo will be a "live" document that will be improved on by those who find bits of it missing. Pull requests are therefore most welcome.

### Software Installation etc.,

It is possible to use your UNIX-Like Operating System installation. The instructor is happy to discuss this outside of the workshop. However to allow for consistency we will be using a virtual machine to allow for consistency. It is my experience that the less hoops one has to jump through when learning the easier it is to comprehend the scope and applicability of concepts. 

### Code of Conduct

Attendees at this workshop are expected to abide by the [Code4lib Code of Conduct](http://code4libnys.github.io/2016/).

In addition I will be applying the Hacker school [social rules](https://www.recurse.com/manual#sub-sec-social-rules) for the entirety of the workshop to foster a healthy learning environment.

### Workshop materials

The overall [agenda is here](AGENDA.md)
