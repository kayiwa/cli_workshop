## script: Record Terminal Session

The format of the `script` command is:

```bash
script [ -a ] [ file ]
```
The `script` command records everything displayed on your terminal to a file. If no file is specified, the filename `typescript` is used. To end the script session one types `exit`. This is extremely useful in recreating steps run for documentation purposes.

Example usage:

```bash
dritchie@cliwkshp:~$ script
Script started, file is typescript
dritchie@cliwkshp:~$ pwd
/home/dritchie
dritchie@cliwkshp:~$ cal
July 2016
Su Mo Tu We Th Fr Sa
                1  2
 3  4  5  6  7  8  9
10 11 12 13 14 15 16
17 18 19 20 21 22 23
24 25 26 27 28 29 30
31
dritchie@cliwkshp:~$ echo -e "one\ntwo\nthree" | wc -l
3
dritchie@cliwkshp:~$ who
kayiwa   pts/9        2016-07-25 19:37
dritchie@cliwkshp:~$ exit
Script done, file is typescript
```
