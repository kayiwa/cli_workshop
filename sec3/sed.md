## sed: non-interactive editor

`sed` is a **s**tream-oriented **ed**itor. It interprets a script and performs actions on the script. `sed` like many Unix programs, input flows through the program and is directed to standard output. We've already encountered the `sort` command that illustrates this. `sed` commands have the general form:

```bash
sed [options] `command` file(s)
sed [options] -f scriptfile files(s)
```
**Typical uses of `sed` include:**

* Editing one or more files automatically
* Simplifying repetitive edits to multiple files
* Writing conversion programs

Examples usage:

```bash
dritchie@cliwkshp:~$ sed --help
dritchie@cliwkshp:~$ sed s/BSD// < ~/cli_workshop/data_files/operatingsystemlist
dritchie@cliwkshp:~$ sed s/BSD//g < ~/cli_workshop/data_files/operatingsystemlist 
dritchie@cliwkshp:~$ sed s/BSD//g < ~/cli_workshop/data_files/operatingsystemlist > nobsdlist
dritchie@cliwkshp:~$ sed -n '/BSD/ p' ~/cli_workshop/data_files/operatingsystemlist
```
